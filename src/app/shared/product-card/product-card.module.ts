import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductCardComponent } from './product-card.component';
import { BadgeModule } from '../badge/badge.module';
import { ButtonModule } from '../button/button.module';
import { RatingModule } from '../rating/rating.module';

@NgModule({
  declarations: [
    ProductCardComponent,
  ],
  imports: [
    CommonModule,
    BadgeModule,
    ButtonModule,
    RatingModule,
  ],
  exports: [ProductCardComponent],
})
export class ProductCardModule {
}
