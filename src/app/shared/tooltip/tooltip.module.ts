import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TooltipComponent } from './tooltip.component';
import { IconModule } from '../icon/icon.module';

@NgModule({
  declarations: [
    TooltipComponent,
  ],
  imports: [
    CommonModule,
  ],
  exports: [TooltipComponent],
})
export class TooltipModule {
}
